/*
$('.post').find('.interaction').find('.edit').on('click', function(event) {
    event.preventDefault();
    
    var postcontent = event.target.parentNode.parentNode.parentNode.childNodes[1].textContent;
    console.log(postcontent);
    $('#edit-modal').modal();
});
*/

// to edit own posts

var postId = 0;
var postBodyElement = null;

$('.mypost').find('.interaction').find('.edit').on('click', function(event) {
    event.preventDefault();
    
    postBodyElement = event.target.parentNode.parentNode.parentNode.parentNode.parentNode.childNodes[1].childNodes[3].lastChild;
    // $("txtCompletion").val($(this).parent().parent().children()[3].html());
    // postBodyElement = $(event.target).parent().parent().parent().parent().parent().children()[1].children()[3].children()[15]; //find('#mypostcontent');
    // postBodyElement = $('#mypostcontent');
    console.log(postBodyElement);
    var singlepostcontent = postBodyElement.textContent;
    console.log(singlepostcontent);
    postId = event.target.parentNode.parentNode.parentNode.parentNode.parentNode.dataset['postid'];
    $('#post-body').val(singlepostcontent);
    // var singlepostname = event.target.parentNode.parentNode.parentNode.parentNode.parentNode.childNodes[1].childNodes[1].textContent;
    // $('#post-name').val(singlepostname);
    $('#edit-modal').modal();
});

$('#modal-save').on('click', function() {
    $.ajax({
        method: 'POST',
        url: urlEdit,
        data: { content: $('#post-body').val(), postId: postId, _token: token}
    })
    .done(function(msg) {
        $(postBodyElement).text(msg['new_content']);
        // return confirm(message.replace(/(\\n|\\r|\\r\\n)/gm,"\n"))
        $('#edit-modal').modal('hide');
    });
});

var otherPostId = 0;  

//to give an advice
// select a button
$('.otherpost').find('.interaction').find('.giveadvice').on('click', function(event) {
     event.preventDefault();   
    // to pass the id
    otherPostId = event.target.parentNode.parentNode.parentNode.parentNode.parentNode.dataset['otherpostid'];
    // open a modal window
    $('#advice-modal').modal();
});
// to save a comment
$('#modal-save-advice').on('click', function() {

    $.ajax({
        method: 'POST',
        url: urlAdvice,
        data: {advice: $('#advice-body').val(), postId: otherPostId, _token: token},
        success: function(){    
            location.reload();   
        },
        error: function (advice) {
                alert("Please fill in the field");
        }
    })

    .done(function(msg) {
        $('#advice-body').text(msg['advice']);
        $('#advice-modal').modal('hide');
    });
    console.log($('#advice-body').val());
    console.log(otherPostId);
});

// to evaluate other posts
$('.otherpost').find('.interaction').find('.evaluate').on('click', function(event) {
    event.preventDefault();
    otherPostId = event.target.parentNode.parentNode.parentNode.parentNode.parentNode.dataset['otherpostid'];
    $('#evaluate-modal').modal();
});

$('#modal-save-evaluate').on('click', function() {

   // var evaluatedata = [parseInt($('#interesting').val()), parseInt($('#easy_read').val()), parseInt($('#new_point').val())];
    $.ajax({
        method: 'POST',
        url: urlEvaluate,
        data: {interesting: $('input[name=valinteresting]:checked', '#interesting').val(), easy_read: $('input[name=val_easy_read]:checked', '#easy_read').val(), new_point: $('input[name=val_new_point]:checked', '#new_point').val(), postId: otherPostId, _token: token},
        success: function(){    
            location.reload();   
        }
    })
 
    .done(function() {
        $('#evaluate-modal').modal('hide');
    });
    console.log($('input[name=valinteresting]:checked', '#interesting').val());
    console.log($('input[name=val_easy_read]:checked', '#easy_read').val());
    console.log($('input[name=val_new_point]:checked', '#new_point').val());
    console.log(otherPostId);
    console.log(token);
});