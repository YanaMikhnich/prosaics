@extends('layouts.app')

@section('title')
    My Posts
@endsection

@section('css')
    <link rel="stylesheet" href="{{ URL::secure('css/posts.css') }}">
@endsection

@section('content')

<section class="row evaluations">
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-success">
            <div class="panel-heading">Evaluation</div>
            <div class="panel-body">
                <form role="form">
                <div class="interesting" value="{{ Request::old('text_type') }}">
                    <label for="interesting">Interesting</label>
                    <label class="radio-inline"><input type="radio" name="interesting">10</label>
                    <label class="radio-inline"><input type="radio" name="interesting">9</label>
                    <label class="radio-inline"><input type="radio" name="interesting">8</label>
                    <label class="radio-inline"><input type="radio" name="interesting">7</label>
                    <label class="radio-inline"><input type="radio" name="interesting">6</label>
                    <label class="radio-inline"><input type="radio" name="interesting">5</label>
                    <label class="radio-inline"><input type="radio" name="interesting">4</label>
                    <label class="radio-inline"><input type="radio" name="interesting">3</label>
                    <label class="radio-inline"><input type="radio" name="interesting">2</label>
                    <label class="radio-inline"><input type="radio" name="interesting">1</label>
                </div>
                
                <button type="submit" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-ok"></span> Evaluate</button>
            </div>
        </div>
    </div>
</section>

@endsection