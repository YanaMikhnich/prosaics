@extends('layouts.app')

@section('title')
    Home
@endsection

@section('content')
<div class="container">
    <div align="center">
        <h5>You are logged in!</h5>
        <a href="/"><img alt="Prosaics" src="/img/main_prosaics1.png"/></a>
    </div>
</div>
@endsection