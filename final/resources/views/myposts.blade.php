@extends('layouts.app')

@section('title')
    My Posts
@endsection

@section('css')
    <link rel="stylesheet" href="{{ URL::secure('css/posts.css') }}">
@endsection

@section('content')
@include('includes.message')

<section class="row posts">
    <div class="col-md-12 col-md-offset-3"
        <header><h4>My Posts:</h4></header>
        @foreach($posts as $post)
          @if(Auth::user() == $post->user)
              <article class="post">
                  <h5>{{ $post->text_name }}</h5>
                  <p>{{ $post->text_type }}</p>
                  <div class="info">
                      Posted on {{ $post->created_at }}
                  </div>
                  <div class="interaction">
                      <a href="{{ route('mypost', ['post_id' => $post->id]) }}"><button type="submit" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-book"></span> View the post</button></a>
                      <!-- <a href="#" class="edit"><button type="submit" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-edit"></span> Edit</button></a> -->
                      <!-- <a href="{{ route('deletepost', ['post_id' => $post->id]) }}"><button type="submit" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-erase"></span> Delete</button></a> -->
                  </div>
              </article>
          @endif
        @endforeach
    </div>
</section>

<!-- <div class="modal fade" tabindex="-1" role="dialog" id="edit-modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edit Post</h4>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="post-body">Edit the Post</label>
            <textarea class="form-control" name="post-body" id="post-body" rows="15"></textarea>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-success">Save changes</button>
      </div> -->
    <!-- </div> --><!-- /.modal-content -->
  <!-- </div> --><!-- /.modal-dialog -->
<!--</div> --><!-- /.modal --> 

@endsection