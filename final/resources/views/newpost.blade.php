@extends('layouts.app')

@section('title')
    New Post
@endsection

@section('content')
@include('includes.message')
    <div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-success">
            <div class="panel-heading"><span class="glyphicon glyphicon-pencil"></span> Add a post</div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-12">
                        <form action="{{ URL::route('createpost') }}" method="post">
                            <div class="form-group {{ $errors->has('text_name') ? 'has-error' : '' }}">
                                <input name="text_name" id="text_name" class="form-control" type="text" placeholder="Post Name" value="{{ Request::old('text_name') }}">
                            </div>
                            <div class="form-group">
                                <p>Select your text type: </p>
                                <select name="text_type" id="text_type" value="{{ Request::old('text_type') }}">
                                    <option value="novel" @if (old('text_type') == 'novel') selected="selected" @endif>Novel</option>
                                    <option value="narrative" @if (old('text_type') == 'narrative') selected="selected" @endif>Narrative</option>
                                    <option value="tale" @if (old('text_type') == 'tale') selected="selected" @endif>Tale</option>
                                    <option value="adventure story" @if (old('text_type') == 'adventure story') selected="selected" @endif>Adventure story</option>
                                    <option value="short story" @if (old('text_type') == 'short story') selected="selected" @endif>Short story</option>
                                </select>
                            </div>
                            <div class="form-group {{ $errors->has('content') ? 'has-error' : '' }}">
                                <textarea name="content" class="form-control" placeholder="Write here your post">{{ Request::old('content') }}</textarea>
                            </div>
                            <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Create Post</button>
                            <input type="hidden" value="{{ Session::token() }}" name="_token">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection