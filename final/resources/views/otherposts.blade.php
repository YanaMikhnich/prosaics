@extends('layouts.app')

@section('title')
    People Posts
@endsection

@section('css')
    <link rel="stylesheet" href="{{ URL::secure('css/posts.css') }}">
@endsection

@section('content')

<section class="row posts">
    <div class="col-md-12 col-md-offset-3"
        <header><h4>Read other people posts to evaluate and give your advice:</h4></header>
        @foreach($posts as $post)
        @if(Auth::user() != $post->user)
            <article class="post">
                <h5>{{ $post->text_name }}</h5>
                <p>{{ $post->text_type }}</p>
                <div class="info">
                    Posted by {{ $post->user->name }} on {{ $post->created_at }}
                </div>
                <a href="{{ route('otherpost', ['post_id' => $post->id]) }}"><button type="submit" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-book"></span> Read the post</button></a>
            </article>
        @endif
        @endforeach
    </div>
</section>

@endsection