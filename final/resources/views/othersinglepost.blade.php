@extends('layouts.app')

@section('title')
    My Posts
@endsection

@section('css')
    <link rel="stylesheet" href="{{ URL::secure('css/posts.css') }}">
@endsection

@section('content')
@include('includes.message')

<section class="row singlepost">
    <div class="col-md-10 col-md-offset-1">
        <article class="otherpost" data-otherpostid="{{ $post->id }}">
            <div class="panel panel-success">
                <div class="panel-heading">{{ $post->text_name }}</div>
                <div class="panel-body">
                    <p>{{ $post->text_type }}</p>
                    <div class="info">
                        Posted by {{ $post->user->name }} on {{ $post->created_at }}
                    </div>
                    <p align="justify">{!! str_replace("\n","<br />", $post->content) !!}</p>
                    <div class="interaction">
                        
                        @if(Auth::user()->evaluation()->where('post_id', $post->id)->first())
                        <button type="submit" class="btn btn-success btn-sm" id="ev_button"><span class="glyphicon glyphicon-ok-circle"></span> Already evaluated</button>
                        @else
                        <a href="#" class="evaluate"><button type="submit" class="btn btn-success btn-sm" id="change_btn"><span class="glyphicon glyphicon-stats"></span> Evaluate</button></a>
                        @endif
                        
                        <a href="#" class="giveadvice"><button type="submit" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-comment"></span> Give your advice</button></a>
                    </div>
                    <div class="well well-sm">Other advices:</div>
                    @if($countadvices > 0)
                    @foreach($advices as $advice)
                        <article class="advice">
                           <p align="justify">{!! str_replace("\n","<br />", $advice->advice) !!}</p>
                            <div class="info">
                                 Posted by {{ $advice->user->name }} on {{ $advice->created_at }}
                            </div>
                        </article>
                    @endforeach
                    @else
                        <p><i>You can be the first person who gives an advice!</i></p>
                    @endif
                    
                </div>
            </div>
        </article>
    </div>
</section>

<div class="modal fade" tabindex="-1" role="dialog" id="advice-modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Give an Advice</h4>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group {{ $errors->has('advice') ? 'has-error' : '' }}">
            <label for="advice-body">Give your advice: </label>
            <textarea class="form-control" name="advice-body" id="advice-body" rows="15"></textarea>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-success" id="modal-save-advice">Post advice</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" tabindex="-1" role="dialog" id="evaluate-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Evaluate Post</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="interesting">Interesting: </label>
                    <form role="form" name="interesting" id="interesting">
                        <label class="radio-inline"><input type="radio" name="valinteresting" value="10">10</label>
                        <label class="radio-inline"><input type="radio" name="valinteresting" value="9">9</label>
                        <label class="radio-inline"><input type="radio" name="valinteresting" value="8">8</label>
                        <label class="radio-inline"><input type="radio" name="valinteresting" value="7">7</label>
                        <label class="radio-inline"><input type="radio" name="valinteresting" value="6">6</label>
                        <label class="radio-inline"><input type="radio" name="valinteresting" value="5">5</label>
                        <label class="radio-inline"><input type="radio" name="valinteresting" value="4">4</label>
                        <label class="radio-inline"><input type="radio" name="valinteresting" value="3">3</label>
                        <label class="radio-inline"><input type="radio" name="valinteresting" value="2">2</label>
                        <label class="radio-inline"><input type="radio" name="valinteresting" value="1">1</label>
                    </form>
                </div>
                <hr>
                <div class="form-group">
                    <label for="easy_read">Easy to read:  </label>
                    <form role="form" name="easy_read" id="easy_read">
                        <label class="radio-inline"><input type="radio" name="val_easy_read" value="10">10</label>
                        <label class="radio-inline"><input type="radio" name="val_easy_read" value="9">9</label>
                        <label class="radio-inline"><input type="radio" name="val_easy_read" value="8">8</label>
                        <label class="radio-inline"><input type="radio" name="val_easy_read" value="7">7</label>
                        <label class="radio-inline"><input type="radio" name="val_easy_read" value="6">6</label>
                        <label class="radio-inline"><input type="radio" name="val_easy_read" value="5">5</label>
                        <label class="radio-inline"><input type="radio" name="val_easy_read" value="4">4</label>
                        <label class="radio-inline"><input type="radio" name="val_easy_read" value="3">3</label>
                        <label class="radio-inline"><input type="radio" name="val_easy_read" value="2">2</label>
                        <label class="radio-inline"><input type="radio" name="val_easy_read" value="1">1</label>
                    </form>
                </div>
                <hr>
                <div class="form-group">
                    <label for="new_point">New point of view:  </label>
                    <form role="form" name="new_point" id="new_point">
                        <label class="radio-inline"><input type="radio" name="val_new_point" value="10">10</label>
                        <label class="radio-inline"><input type="radio" name="val_new_point" value="9">9</label>
                        <label class="radio-inline"><input type="radio" name="val_new_point" value="8">8</label>
                        <label class="radio-inline"><input type="radio" name="val_new_point" value="7">7</label>
                        <label class="radio-inline"><input type="radio" name="val_new_point" value="6">6</label>
                        <label class="radio-inline"><input type="radio" name="val_new_point" value="5">5</label>
                        <label class="radio-inline"><input type="radio" name="val_new_point" value="4">4</label>
                        <label class="radio-inline"><input type="radio" name="val_new_point" value="3">3</label>
                        <label class="radio-inline"><input type="radio" name="val_new_point" value="2">2</label>
                        <label class="radio-inline"><input type="radio" name="val_new_point" value="1">1</label>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" id="modal-save-evaluate">Evaluate</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
    var token = '{{ Session::token() }}';
    var urlEvaluate = '{{ route('evaluate') }}';
    var urlAdvice = '{{ route('advice') }}';
</script>

@endsection