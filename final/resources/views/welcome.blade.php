@extends('layouts.app')

@section('title')
    Welcome!
@endsection

@section('content')
<div class="container">
    <div align="center">
        <a href="/"><img alt="Prosaics" src="/img/main_prosaics1.png"/></a>
    </div>
</div>
@endsection
