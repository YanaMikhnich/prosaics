@extends('layouts.app')

@section('title')
    My Posts
@endsection

@section('css')
    <link rel="stylesheet" href="{{ URL::secure('css/posts.css') }}">
@endsection

@section('content')
@include('includes.message')

<section class="row singlepost">
    <div class="col-md-10 col-md-offset-1">
        <article class="mypost" data-postid="{{ $post->id }}">
            <div class="panel panel-success">
                <div class="panel-heading">{{ $post->text_name }}</div>
                <div class="panel-body">
                    <p>{{ $post->text_type }}</p>
                    <div class="info">
                        Posted on {{ $post->created_at }}
                    </div>
                    <div class="interaction">
                        <a href="#" class="edit"><button type="submit" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-edit"></span> Edit</button></a>
                        <a href="{{ route('deletepost', ['post_id' => $post->id]) }}"><button type="submit" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-erase"></span> Delete</button></a>
                    </div>
                    <div class="evaluation">
                        <div class="well well-sm">Evaluation:</div>
                        @if($easy_av > 0)
                        <div class="row">
                            <div class="col-xs-3 col-md-2">Easy to read: </div>
                            <div class="col-xs-2 col-md-1"><span class="badge">{{ number_format(round($easy_av,2), 2, '.', '') }}</span></div>
                        </div>
                         <div class="row">
                            <div class="col-xs-3 col-md-2">Interesting story: </div>
                            <div class="col-xs-2 col-md-1"><span class="badge">{{ number_format(round($interesting_av,2), 2, '.', '') }}</span></div>
                        </div>
                         <div class="row">
                            <div class="col-xs-3 col-md-2">New point: </div>
                            <div class="col-xs-2 col-md-1"><span class="badge">{{ number_format(round($new_av,2), 2, '.', '') }}</span></div>
                        </div>
                        @else
                        <p><i>Unfortunately people haven't avaluated your post yet.</i></p>
                        @endif
                    </div>
                    <div class="well well-sm">Advices:</div>
                    @if($countadvices > 0)
                    @foreach($advices as $advice)
                    <article class="advice">
                        <p align="justify">{!! str_replace("\n","<br />", $advice->advice) !!}</p>
                        <div class="info">
                            Posted by {{ $advice->user->name }} on {{ $advice->created_at }} 
                        </div>
                    </article>
                    @endforeach
                    @else
                        <p><i>Unfortunately you don't have any advices.</i></p>
                    @endif
                    <hr><span>{!! str_replace("\n","<br />", $post->content) !!}</span></div>
            </div>
        </article>
    </div>
</section>

<div class="modal fade" tabindex="-1" role="dialog" id="edit-modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edit Post</h4>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <!-- <label for="post-name">Edit the Post Name</label> -->
            <!-- <textarea class="form-control" name="post-name" id="post-name" rows="2"></textarea> -->
            <label for="post-body">Edit the Post</label>
            <textarea class="form-control" name="post-body" id="post-body" rows="15"></textarea>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-success" id="modal-save">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
    var token = '{{ Session::token() }}';
    var urlEdit = '{{ route('edit') }}';
</script>
    
@endsection