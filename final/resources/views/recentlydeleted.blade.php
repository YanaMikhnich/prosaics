@extends('layouts.app')

@section('title')
    Recently Deleted
@endsection

@section('css')
    <link rel="stylesheet" href="{{ URL::secure('css/posts.css') }}">
@endsection

@section('content')
@include('includes.message')

<section class="row posts">
    <div class="col-md-12 col-md-offset-3"
        <header><h4>Recently Deleted:</h4></header>
        @foreach($posts as $post)
          @if(Auth::user() == $post->user)
              <article class="post">
                  <h5>{{ $post->text_name }}</h5>
                  <p>{{ $post->text_type }}</p>
                  <p>Part of the text here...</p>
                  <div class="info">
                      Posted on {{ $post->created_at }}
                  </div>
                    <div class="interaction">
                        <a href="#"><button type="submit" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-thumbs-up"></span> Move to My Posts</button></a>
                        <a href="{{ route('deletepost', ['post_id' => $post->id]) }}"><button type="submit" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-erase"></span> Delete forever</button></a>
                    </div>
              </article>
          @endif
        @endforeach
    </div>
</section>


@endsection