<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
    public function evaluation()
    {
        return $this->hasMany('App\Evaluation');
    }
    
    public function advice()
    {
        return $this->hasMany('App\Advice');
    }
    
    /*
    public function getAvgEvaluation()
    {
        $int = $this->evaluations('interesting')->avg();
        $easy = $this->evaluations('easy_read')->avg();
        $new = $this->evaluations('new_point')->avg();
        $average_mark = round(($int + $easy + $new)/3, 2);
        return $average_mark;
    }
    */
}