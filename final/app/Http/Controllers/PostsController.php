<?php

namespace App\Http\Controllers;

use App\Posts;
use App\Advice;
use App\Evaluation;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Illuminate\Support\Facades\Response;
// use Illuminate\Foundation\Auth\ThrottlesLogins;
// use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
// use  App\Http\Controllers\Auth;

class PostsController extends Controller
{
    // Create a post
    public function createPost(Request $request)
    {
        // validation
        $this->validate($request, [
            'text_name' => 'required|max:255',
            'text_type' => 'required',
            'content' => 'required',
        ]);
        
        // create a new post
        $post = new Posts();
        $post->text_name = $request['text_name'];
        $post->text_type = $request['text_type'];
        $post->content = $request['content'];
        $message = 'Please fill out all fields';
        if ($request->user()->posts()->save($post)) {
            $message = 'Your post has been successfully created!';
        }
        return redirect('myposts')->with(['message' => $message]);
    }
    
    // to view the form for the new post
    public function newPost()
    {
        return view('newpost');
    }
    
    // to view a user's posts
    public function vewMyPosts()
    {
        $posts = Posts::orderBy('created_at', 'desc')->get(); // $posts = Posts::all(); - for all without sorted by time
        $ev = Evaluation::all();
        // $post_id = Posts::select('id')->get();
        // $posts_id = Posts::where('evaluations.post_id', 'posts.id');
        //$average_mark = 0.000;
        //if($average_new > 0)
        //{
        // foreach($posts as $post)
        // {
            // $posts = Posts::all();
            // $average_int = Evaluation::where('evaluations.post_id', 'posts.id')->avg('interesting');
            // $average_easy = Evaluation::where('evaluations.post_id', 'posts.id')->avg('easy_read');
            // $average_new = Evaluation::where('evaluations.post_id', 'posts.id')->avg('new_point');
            // $average_int = Evaluation::avg('interesting');
            // $average_easy = Evaluation::avg('easy_read');
            // $average_new = Evaluation::avg('new_point');
            /*
            $average_int = Posts::whereHas('evaluation', function($query) {
                $query->avg('interesting');
            })->get();
            $average_int = Posts::whereHas('evaluation', function($query) {
                $query->avg('easy_read');
            })->get();
            $average_int = Posts::whereHas('evaluation', function($query) {
                $query->avg('new_point');
            })->get();
            */
            /*
            $average_int = DB::table('posts')
                ->join('evaluations', 'posts.id', '=', 'evaluations.post_id')
                // ->where('evaluations.post_id', '=', 'posts.id')
                ->avg('interesting');
            $average_new = DB::table('posts')
                ->join('evaluations', 'posts.id', '=', 'evaluations.post_id')
                // ->where('posts.id', '=', 'evaluations.post_id')
                ->avg('new_point');
            $average_easy = DB::table('posts')
                ->join('evaluations', 'posts.id', '=', 'evaluations.post_id')
                // ->where('posts.id', '=', 'evaluations.post_id')
                ->avg('easy_read');
            */
            /*
            $comments = Advice::where('advices.post_id', '=', 'posts.id')->get();
            if($comments)
            {
                $comcount = $comments->count();
            }
            */
            // <span class="badge">{{ $average_mark }}</span>
            
            
            /*$eval = DB::table('posts')
                ->join('evaluations', 'posts.id', '=', 'evaluations.post_id')
                // ->where('evaluations.post_id', '=', 'posts.id')
                ->get();
            $average_int = $eval->interesting;
            */    
            // $average_mark = round(($average_int + $average_easy + $average_new)/3, 2);
        // }
        return view('myposts', ['posts' => $posts, 'ev' => $ev]);
    }
    
    // to view all other posts
    public function vewOtherPosts()
    {
        $posts = Posts::orderBy('created_at', 'desc')->get();
        return view('otherposts', ['posts' => $posts]);
    }
    
    // to view user's own post
    public function viewMyPost($post_id)
    {
        $post = Posts::where('id', $post_id)->first(); // can also use: $posts = Posts::find($post_id)->first(); $posts = Posts::where('id', '<', $post_id)->first();  = is default
        $advices = Advice::where('post_id', $post_id)->orderBy('created_at', 'desc')->get();
        $countadvices = $advices->count();
        $interesting_av = Evaluation::where('post_id', $post_id)->avg('interesting');
        $easy_av = Evaluation::where('post_id', $post_id)->avg('easy_read');
        $new_av = Evaluation::where('post_id', $post_id)->avg('new_point');
        return view('mysinglepost', ['post' => $post, 'advices' => $advices, 'countadvices' => $countadvices, 'interesting_av' => $interesting_av, 'easy_av' => $easy_av, 'new_av' => $new_av]);
    }
    
    // to view someone's one post
    public function viewOtherPost($post_id)
    {
        $post = Posts::where('id', $post_id)->first(); // can also use: $posts = Posts::find($post_id)->first(); $posts = Posts::where('id', '<', $post_id)->first();  = is default
        $advices = Advice::where('post_id', $post_id)->orderBy('created_at', 'desc')->get();
        $countadvices = $advices->count();
        return view('othersinglepost', ['post' => $post, 'advices' => $advices, 'countadvices' => $countadvices]);
    }
    
    
    // to delete user's own post
    public function deletePost($post_id, Request $request)
    {
        $post = Posts::where('id', $post_id)->first(); // can also use: $posts = Posts::find($post_id)->first(); $posts = Posts::where('id', '<', $post_id)->first();  = is default
        if ($request->user() != $post->user) { // if (Auth::user() != $post->user) doesn't work
            return redirect()->back();
        }
        $post->delete();
        return redirect()->route('myposts')->with(['message' => 'Successfully deleted!']);
    }

/*
    // to view posts from trash
    public function viewRecentlyDeleted()
    {
        $posts = Posts::orderBy('created_at', 'desc')->get(); // $posts = Posts::all(); - for all without sorted by time
        return view('recentlydeleted', ['posts' => $posts]);
    }
*/

    // to edit user's own post
    public function editPost(Request $request)
    {
        $this->validate($request, [
            'content' => 'required'
        ]);
        $post = Posts::find($request['postId']);
        if ($request->user() != $post->user) {
            return redirect()->back();
        }
        $post->content = str_replace("\n","<br />", $request['content']);
        $post->update();
        return response()->json(['new_content' => $post->content], 200);
    }
    
    // to comment someone's post
    public function advice(Request $request)
    {
        $post_id = $request['postId'];
        // validation
        $this->validate($request, [
            'advice' => 'required'
        ]);
        
        $post = Posts::find($post_id);
        if (!$post)
        {
            return null;
        }
        
        $user = $request->user();

        // create a new comment
        $advice = new Advice();
        $advice->advice = $request['advice'];
        $advice->user_id = $user->id;
        $advice->post_id = $post_id; // or $post->id
        $advice->save();
        return response()->json(['advice' => $advice->advice], 200);
    }
    
    // to evaluate someone's post
    public function evaluatePost(Request $request)
    {
        $post_id = $request['postId'];
        // validation
        $this->validate($request, [
            'interesting' => 'required',
            'easy_read' => 'required',
            'new_point' => 'required',
        ]);
        
        $post = Posts::find($post_id);
        if (!$post)
        {
            return null;
        }
        
        $user = $request->user();
        
        // to not allow avaluate own posts
        if ($user == $post->user) {
            return redirect()->back();
        }
        
        // to not allow avaluate twice
        $isevaluated = $request['interesting'];

        // to add evaluations to the db
        $evaluate = new Evaluation();
        $evaluate->interesting = (int)($request['interesting']);
        $evaluate->easy_read = (int)($request['easy_read']);
        $evaluate->new_point = (int)($request['new_point']);
        $evaluate->user_id = $user->id;
        $evaluate->post_id = $post->id;
        $evaluate->save();

        return response()->json(['interesting' => $evaluate->interesting, 'easy_read' => $evaluate->easy_read, 'new_point' => $evaluate->new_point], 200);
    }
}