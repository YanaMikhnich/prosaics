<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

// Route::get('/home', 'HomeController@index');
Route::get('/home', [
    'uses' => 'HomeController@index',
    'as' => 'home',
    'middleware' => 'auth'
]);


// Route::get('/newpost', function () {
    // return view('newpost');
// });

Route::get('/newpost', array (
    'uses' => 'PostsController@newPost',
    'as' => 'newpost',
    'middleware' => 'auth'
));

Route::get('/myposts', [
    'uses' => 'PostsController@vewMyPosts',
    'as' => 'myposts',
    'middleware' => 'auth'
]);

Route::get('/otherposts', [
    'uses' => 'PostsController@vewOtherPosts',
    'as' => 'otherposts',
    'middleware' => 'auth'
]);

Route::post('/createpost', array (
    'uses' => 'PostsController@createPost',
    'as' => 'createpost',
    'middleware' => 'auth'
));

Route::get('/mypost/{post_id}', [
    'uses' => 'PostsController@viewMyPost',
    'as' => 'mypost',
    'middleware' => 'auth'
]);

Route::get('/otherpost/{post_id}', [
    'uses' => 'PostsController@viewOtherPost',
    'as' => 'otherpost',
    'middleware' => 'auth'
]);

Route::get('/deletepost/{post_id}', [
    'uses' => 'PostsController@deletePost',
    'as' => 'deletepost',
    'middleware' => 'auth'
]);

Route::post('/edit', [
    'uses' => 'PostsController@editPost',
    'as' => 'edit'
]);

/*
Route::get('/evaluation/{post_id}', [
    'uses' => 'PostsController@viewEvaluation',
    'as' => 'evaluation',
    'middleware' => 'auth'
]);
*/

Route::post('/evaluate', [
    'uses' => 'PostsController@evaluatePost',
   'as' => 'evaluate'
]);

Route::post('/advice', [
   'uses' => 'PostsController@advice',
   'as' => 'advice'
]);

/*
Route::get('/recentlydeleted', [
    'uses' => 'PostsController@viewRecentlyDeleted',
    'as' => 'recentlydeleted',
    'middleware' => 'auth'
]);

Route::post('/trash', [
    'uses' => 'PostsController@toTrash',
    'as' => 'trash'
]);
*/